-- Find all artists that has a letter d in its name

SELECT * FROM artists WHERE name LIKE "%d%";

-- Find all songs that has a length of less than 230

SELECT * FROM songs WHERE length < 230;

-- Join the albums songs and tables, showing only album name, song name, song length

SELECT album_title, song_name, length FROM albums
    JOIN songs ON albums.id = songs.album_id;

-- Join artist and album tables, find all albums with a letter a in its name;

SELECT album_title, name FROM albums 
    JOIN artists ON artists.id = albums.artist_id
    WHERE album_title LIKE "%a%";

-- sort the albums in z-a order limiting to 4 results

SELECT album_title FROM albums ORDER BY album_title DESC LIMIT 4;

-- Join albums and songs table, sort albums from z-a and songs from a-z

SELECT album_title, song_name FROM albums
    JOIN songs ON albums.id = songs.album_id
    ORDER BY album_title DESC, song_name;
